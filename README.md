## Sidecar Pod

### Opis pliku

1. Sekcja tworząca namespace

Sekcja oddzielona przez "---", aby uruchomić ją osobno.
Tworzy namespace o nazwie "lab3"

```
---
...
metadata:
  creationTimestamp: null
  name: lab3
...
---
```

2. Sekcja tworząca pod'a

- Wykorzystanie utworzonego namespace:

```
metadata:
  ...
  namespace: lab3
```

- Kontener busybox:

```
containers:
  - image: busybox
    name: sidecar-pod
    command: ["/bin/sh","-c"]
    args:
    - while true; do
        date
        >> /var/log/date.log;
        sleep 5;
      done
    volumeMounts:
      - mountPath: /var/log/date.log
        name: date-share
```

Zawiera wywołanie komendy /bin/sh -c, która przyjmuje argument w postaci nieskończonej pętli while - w niej następuje wywołanie komendy date oraz przekierowanie wyjscia do pliku /var/log/date.log z nadpisaniem.

Kontener zawiera również podmontowanie niżej opisanego woluminu w w/w pliku - mountPath oznacza ścieżkę montowania, a name nazwę wykorzystanego woluminu

- Kontener nginx

```
- image: nginx
    name: nginx
    volumeMounts:
      - mountPath: /usr/share/nginx/html/index.html
        name: date-share
```

Zawiera podmontowanie w ścieżce /usr/share/nginx/html/index.html. Można powiedzieć, że date.log jest zamieniany w index.html.

- Deklaracja woluminów

```
volumes:
    - name: date-share
      hostPath:
        path: /var/local/date.log
        type: FileOrCreate
```

Składa się z nazwy - 'date-share' oraz typu woluminu 'hostPath'. Montuje on plik lub katalog z systemu plików węzła hosta w podzie. Następnie podana jest ścieżka oraz typ "FileOrCreat', który oznacza, że jeśli nic nie istnieje w podanej ścieżce, pusty plik zostanie utworzony w razie potrzeby z uprawnieniami ustawionymi na 0644.

### Poprawność działania

1. Stworzenie/uruchomienie pod'a
![kubectl apply](/images/apply.png "Kubectl Apply")

2. Port-forwarding
![port-forwarding](/images/port-forwarding.png "Port-forwarding")

3. Wynik
![results](/images/result.png "Results")

